package main

// example of cocopacket to prometheus connector
// handling requests like http://127.0.0.1:8008/GROUP/SLAVE for one group/slave
// or http://127.0.0.1:8008 for getting all IPs at once

import (
	"context"
	"flag"
	"fmt"
	"log"
	"net/http"
	"os"
	"os/signal"
	"strings"
	"syscall"
	"time"

	api "gitlab.com/coco_packet/cocopacket-api"
)

var (
	url    = flag.String("url", "", "URL of cocopacket master instance")
	user   = flag.String("user", "", "username for authorization")
	passwd = flag.String("password", "", "password for authorization")
	listen = flag.String("listen", "0.0.0.0:8008", "ip:port to listen for prometheus requests")
	extra  = flag.String("extra", ", department=\"cocopacket\"", "additional string metrics")
)

func stringEscape(value string) string {
	return "\"" + strings.ReplaceAll(value, "\"", "'") + "\""
}

type prometheusHandler struct{}

func (prometheusHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	parts := strings.Split(strings.Trim(r.URL.RequestURI(), "/"), "/")
	if (2 != len(parts)) && !(1 == len(parts) && parts[0] == "") {
		// invalid URL - we need /GROUP/SLAVE
		w.WriteHeader(http.StatusNotAcceptable)
		return
	}

	w.Header().Add("Content-type", "text/plain")

	// ignoring second part, we don't need stats about http requests
	var (
		pings map[string]*api.AvgChunk
		err   error
	)

	switch len(parts) {
	case 1:
		pings, _, err = api.LastStats()
	case 2:
		pings, _, err = api.GroupLastStats(parts[0], parts[1])
	}
	if nil != err {
		w.Write([]byte("# error loading cocopacket stats: " + err.Error()))
		return
	}

	config, err := api.GetConfigInfo()
	if nil != err {
		w.Write([]byte("# error loading cocopacket config: " + err.Error()))
		return
	}

	w.Write([]byte("# HELP cocopacket_latency in ms\n# TYPE cocopacket_latency gauge\n"))
	for ip, data := range pings {
		slave := ""
		parts := strings.Split(ip, "@")
		if len(parts) > 1 {
			ip = parts[0]
			slave = ", slave=" + stringEscape(parts[1])
		}
		categories := ""
		if len(config.Ping.IPs[ip].Groups) > 0 {
			categories = ", categories=" + stringEscape(strings.TrimSuffix(config.Ping.IPs[ip].Groups[0], "->"))
		}

		if 0 == data.Count {
			w.Write([]byte(fmt.Sprintf("cocopacket_latency{ip=\"%s\"%s%s, description=%s%s} 0\n",
				ip, slave, categories, stringEscape(config.Ping.IPs[ip].Description), *extra)))
		} else {
			w.Write([]byte(fmt.Sprintf("cocopacket_latency{ip=\"%s\"%s%s, description=%s%s} %.2f\n",
				ip, slave, categories, stringEscape(config.Ping.IPs[ip].Description), *extra, data.Latency/float32(data.Count))))
		}
	}

	w.Write([]byte("# HELP cocopacket_packet_loss_percent\n# TYPE cocopacket_packet_loss_percent gauge\n"))
	for ip, data := range pings {
		slave := ""
		parts := strings.Split(ip, "@")
		if len(parts) > 1 {
			ip = parts[0]
			slave = ", slave=" + stringEscape(parts[1])
		}
		categories := ""
		if len(config.Ping.IPs[ip].Groups) > 0 {
			categories = ", categories=" + stringEscape(strings.TrimSuffix(config.Ping.IPs[ip].Groups[0], "->"))
		}

		if 0 == data.Loss {
			w.Write([]byte(fmt.Sprintf("cocopacket_packet_loss_percent{ip=\"%s\"%s%s, description=%s%s} 0\n",
				ip, slave, categories, stringEscape(config.Ping.IPs[ip].Description), *extra)))
		} else {
			w.Write([]byte(fmt.Sprintf("cocopacket_packet_loss_percent{ip=\"%s\"%s%s, description=%s%s} %.2f\n",
				ip, slave, categories, stringEscape(config.Ping.IPs[ip].Description), *extra, float64(data.Loss)/float64(data.Count)*100)))
		}
	}
}

func main() {
	flag.Parse()

	if "" == *url {
		fmt.Println("Please specify URL of cocopacket master")
		flag.Usage()
		return
	}

	api.Init(*url, *user, *passwd)

	srv := http.Server{
		Handler: prometheusHandler{},
		Addr:    fmt.Sprintf(*listen),
	}

	c := make(chan os.Signal, 1)
	signal.Notify(c, syscall.SIGINT)
	signal.Notify(c, syscall.SIGTERM)
	go func() {
		<-c
		// sig is a ^C, handle it
		fmt.Println("shutting down..")

		// create context with timeout
		ctx, cancel := context.WithTimeout(context.Background(), 1*time.Second)
		defer cancel()

		// start http shutdown
		srv.Shutdown(ctx)

		// verify, in worst case call cancel via defer
		select {
		case <-time.After(2 * time.Second):
			fmt.Println("not all connections done")
			os.Exit(0)
		case <-ctx.Done():

		}
	}()

	log.Println(srv.ListenAndServe())
}
